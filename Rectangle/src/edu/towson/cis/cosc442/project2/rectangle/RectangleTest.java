package edu.towson.cis.cosc442.project2.rectangle;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The unit test Class for Rectangle.
 */
public class RectangleTest {
	
	/** Declaring necessary test objects for {@link Rectangle} */
	Rectangle rect1, rect2;

	/**
	 * Initializes the necessary test objects for the test cases to use.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		rect1 = new Rectangle(new Point(2.0, 2.0), new Point(4.0, 7.0));
		rect2 = new Rectangle(new Point(2.0, 6.0), new Point(4.0, 3.0));
	}

	/**
	 * Test for the getArea() method of the {@link Rectangle} class.
	 */
	@Test
	public void testGetArea() {
		assertEquals(10.0, rect1.getArea(),0.001);
		assertEquals(6.0, rect2.getArea(),0.001);
	}

	/**
	 * Test for the getDiagonal() method of the {@link Rectangle} class.
	 */
	@Test
	public void testGetDiagonal() {
		assertEquals(5.3852, rect1.getDiagonal(), 0.0001);
		assertEquals(3.6056, rect2.getDiagonal(), 0.0001);
	}
	/**
	 * Test for the getX1() method of the {@link Rectangle} class. 
	 */
	@Test
	public void testgetX1() {
		assertEquals(2.0, rect1.getX1(), 0.0001);
		assertEquals(2.0, rect2.getX1(), 0.0001);
	}
	/**
	 * Test for the getX2() method of the {@link Rectangle} class. 
	 */
	@Test
	public void testgetX2() {
		assertEquals(4.0, rect1.getX2(), 0.0001);
		assertEquals(4.0, rect2.getX2(), 0.0001);
	}
	/**
	 * Test for the getY1() method of the {@link Rectangle} class. 
	 */
	@Test
	public void testgetY1() {
		assertEquals(2.0, rect1.getY1(), 0.0001);
		assertEquals(6.0, rect2.getY1(), 0.0001);
	}
	/**
	 * Test for the getY2() method of the {@link Rectangle} class. 
	 */
	@Test
	public void testgetY2() {
		assertEquals(7.0, rect1.getY2(), 0.0001);
		assertEquals(3.0, rect2.getY2(), 0.0001);
	}
	/**
	 * Cleans up test objects after a test case is executed.
	 */
	@After
	public void tearDown(){
		rect1 = null;
		rect2 = null;
	}
}
