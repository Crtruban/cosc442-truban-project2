package edu.towson.cis.cosc442.project2.vendingmachine;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


// TODO: Auto-generated Javadoc
/**
 * The Class VendingMachineItemTest.
 */
public class VendingMachineItemTest {

	/** The v item 2. */
	VendingMachineItem vItem1, vItem2, vItem3, vItem4, vItem5;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		vItem3 = new VendingMachineItem("Starburst", 1.00);
		vItem4 = new VendingMachineItem("Skittles", 1.75);
		
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Tests VendingMachineItem() constructor for {@link VendingMachineItem} class
	 */
	@Test
	public void testVendingMachineItem() {
		//All constructors should assign their appropriate name and price. No exceptions should occur
		vItem1 = new VendingMachineItem("Hershey's Bar", 1.50);
		assertEquals("Hershey's Bar", vItem1.getName());
		assertEquals(1.50, vItem1.getPrice(),0.001);
		vItem2 = new VendingMachineItem("Twix", 2.00);
		assertEquals("Twix", vItem2.getName());
		assertEquals(2.00, vItem2.getPrice(),0.0001);

	}
	/**
	 * Testing for exceptions in the VendingMachineItem() constructor for {@link VendingMachineItem} class
	 */
	
	@Test(expected = VendingMachineException.class)
	public void testExceptionVendingMachineItem() 
	{
//All constructors should have exceptions as all prices are below 0.
		
		vItem5 = new VendingMachineItem("Exceptional", - 3);
		vItem1 = new VendingMachineItem("Exceptional", - .00005);
		vItem2 = new VendingMachineItem("Exceptional", - 1);
		vItem3 = new VendingMachineItem("Exceptional", - 2000);
		
	}
	/**
	 * Tests getName() of {@link VendingMachineItem} class 
	 */
	@Test
	public void testGetName() {
		//Should return appropriate names to their vending machine item object
		assertEquals("Starburst", vItem3.getName());
		assertEquals("Skittles", vItem4.getName());
	}

	/**
	 * Tests getPrice() of {@link VendingMachineItem} class
	 */
	@Test
	public void testGetPrice() {
		//should return the item prices assigned in setUp.
		assertEquals(1.00, vItem3.getPrice(),0.001);
		assertEquals(1.75, vItem4.getPrice(),0.001);
	}

}
